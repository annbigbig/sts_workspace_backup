package com.kashu.demo.repository;

import com.kashu.demo.entity.Order;

public interface OrderRepository {

	  Order save(Order order);
	  
}
