package com.kashu.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KashuTacoCloud3Application {

	public static void main(String[] args) {
		SpringApplication.run(KashuTacoCloud3Application.class, args);
	}

}

