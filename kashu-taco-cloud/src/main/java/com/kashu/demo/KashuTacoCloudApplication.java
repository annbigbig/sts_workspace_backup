package com.kashu.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KashuTacoCloudApplication {

	public static void main(String[] args) {
		SpringApplication.run(KashuTacoCloudApplication.class, args);
	}

}

