package com.kashu.demo.repository;

import org.springframework.data.repository.CrudRepository;
import com.kashu.demo.entity.Taco;

public interface TacoRepository 
         extends CrudRepository<Taco, Long> {

}
