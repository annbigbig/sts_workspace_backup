package com.kashu.demo.repository;

import org.springframework.data.repository.CrudRepository;
import com.kashu.demo.entity.Order;

public interface OrderRepository 
         extends CrudRepository<Order, Long> {

}