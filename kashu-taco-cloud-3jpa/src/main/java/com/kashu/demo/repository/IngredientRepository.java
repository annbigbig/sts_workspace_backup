package com.kashu.demo.repository;

import org.springframework.data.repository.CrudRepository;
import com.kashu.demo.entity.Ingredient;

public interface IngredientRepository 
         extends CrudRepository<Ingredient, String> {

}