package com.kashu.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KashuTacoCloud3jpaApplication {

	public static void main(String[] args) {
		SpringApplication.run(KashuTacoCloud3jpaApplication.class, args);
	}

}
