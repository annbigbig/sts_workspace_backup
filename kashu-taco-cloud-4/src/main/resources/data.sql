delete from Taco_Order_Tacos;
delete from Taco_Ingredients;
delete from Taco;
delete from Taco_Order;
delete from Ingredient;
delete from Users;
delete from Authorities;

insert into Ingredient (id, name, type) 
                values ('FLTO', 'Flour Tortilla', 'WRAP');
insert into Ingredient (id, name, type) 
                values ('COTO', 'Corn Tortilla', 'WRAP');
insert into Ingredient (id, name, type) 
                values ('GRBF', 'Ground Beef', 'PROTEIN');
insert into Ingredient (id, name, type) 
                values ('CARN', 'Carnitas', 'PROTEIN');
insert into Ingredient (id, name, type) 
                values ('TMTO', 'Diced Tomatoes', 'VEGGIES');
insert into Ingredient (id, name, type) 
                values ('LETC', 'Lettuce', 'VEGGIES');
insert into Ingredient (id, name, type) 
                values ('CHED', 'Cheddar', 'CHEESE');
insert into Ingredient (id, name, type) 
                values ('JACK', 'Monterrey Jack', 'CHEESE');
insert into Ingredient (id, name, type) 
                values ('SLSA', 'Salsa', 'SAUCE');
insert into Ingredient (id, name, type) 
                values ('SRCR', 'Sour Cream', 'SAUCE');
                
insert into Users(username,password,enabled,fullname,street,city,state,zip,phoneNumber)
	values('admin','$2a$10$vHlARNscwzsj6ggt2frb8ecmxqsC3r3HNY3AmzwPNaYSClr/Cr6kO',true,'naruto','street 39','konoha','japan','252','0977377377');
insert into Users(username,password,enabled,fullname,street,city,state,zip,phoneNumber)
	values('tony','$2a$10$Tbt/KO/3qIsgD00POUQq5umN58i5/Sf6Qc0vrPYpl1B2IzFQjTUQy',true,'Tony Stark','street 42','New York','USA','251','0988388388');
insert into Users(username,password,enabled,fullname,street,city,state,zip,phoneNumber)
	values('chacha','$2a$10$9n91JifYOZ1P2/B.WL8LFO9sOpXbE0m.YHHR.fo.aArJoYh60wKHK',true,'Cha Cha','street 41','Tokyo','Japan','250','0936336336');	

insert into Authorities(username,authority) 
	values('admin','ROLE_USER');
insert into Authorities(username,authority) 
	values('admin','ROLE_ADMIN');
insert into Authorities(username,authority) 
	values('tony','ROLE_USER');
insert into Authorities(username,authority) 
	values('chacha','ROLE_USER');
insert into Authorities(username,authority) 
	values('chacha','ROLE_ANONYMOUS');	
	
