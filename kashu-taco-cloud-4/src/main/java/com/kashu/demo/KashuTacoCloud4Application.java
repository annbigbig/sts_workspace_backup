package com.kashu.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KashuTacoCloud4Application {

	public static void main(String[] args) {
		SpringApplication.run(KashuTacoCloud4Application.class, args);
	}

}
