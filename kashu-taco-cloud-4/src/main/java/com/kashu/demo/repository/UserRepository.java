package com.kashu.demo.repository;

import com.kashu.demo.entity.User;

public interface UserRepository {
	User findByUsername(String username);
	User save(User user);
}
