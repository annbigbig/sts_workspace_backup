package com.kashu.demo.repository;

import com.kashu.demo.entity.Taco;

public interface TacoRepository  {

	  Taco save(Taco design);
	  
}