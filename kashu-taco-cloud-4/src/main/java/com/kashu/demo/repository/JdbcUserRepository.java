package com.kashu.demo.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.PreparedStatementCreatorFactory;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Repository;

import com.kashu.demo.entity.User;

@Repository
public class JdbcUserRepository implements UserRepository {
	@Autowired
	private JdbcTemplate jdbc;
	
	private final String SQL_LEFT_JOIN = "SELECT u.username, u.password, u.enabled, " +
	  "u.fullname, u.street, u.city, u.state, u.zip, u.phoneNumber, " +
	  "a.authority FROM Users u LEFT JOIN Authorities a ON u.username = a.username " +
	  "WHERE u.username = ? ";
	
	private final String SQL_INSERT_USER = "INSERT INTO Users " +
			"(username,password,enabled,fullname,street,city,state,zip,phoneNumber) " +
			"VALUES (?,?,?,?,?,?,?,?,?)";
	private final String SQL_INSERT_AUTHORITY = "INSERT INTO Authorities (username,authority) "+
				"VALUES (?,?)";

	@Override
	public User findByUsername(String username) {
		return jdbc.query(SQL_LEFT_JOIN, new Object[] {username}, this::mapRowsToUser);
	}
	
	private User mapRowsToUser(ResultSet rs) throws SQLException {
		User user = null;
		List<GrantedAuthority> authorities = new ArrayList<>();
		
		while(rs.next()) {
			if(user==null) {
				user = new User();
				user.setUsername(rs.getString("username"));
				user.setPassword(rs.getString("password"));
				user.setEnabled(rs.getBoolean("enabled"));
				user.setFullname(rs.getString("fullname"));
				user.setStreet(rs.getString("street"));
				user.setCity(rs.getString("city"));
				user.setState(rs.getString("state"));
				user.setZip(rs.getString("zip"));
				user.setPhoneNumber(rs.getString("phoneNumber"));
			}
			authorities.add(new SimpleGrantedAuthority(rs.getString("authority")));
		}
		user.setAuthorities(authorities);
		return user;
	}

	@Override
	public User save(User user) {
		saveUserInfo(user);
		for(GrantedAuthority ga : user.getAuthorities()) {
			saveAuthoritiesForUser(ga,user.getUsername());
		}
		return user;
	}
	
	private void saveUserInfo(User user) {
		Object[] args = {user.getUsername(),
                user.getPassword(),
                user.getEnabled(),
                user.getFullname(),
                user.getStreet(),
                user.getCity(),
                user.getState(),
                user.getZip(),
                user.getPhoneNumber()};
		int[] argTypes = {Types.VARCHAR, Types.VARCHAR, Types.BOOLEAN, Types.VARCHAR,
	            Types.VARCHAR,Types.VARCHAR,Types.VARCHAR,Types.VARCHAR,Types.VARCHAR};
		jdbc.update(SQL_INSERT_USER, args, argTypes);
	}
	
	private void saveAuthoritiesForUser(GrantedAuthority ga, String username) {
		Object[] args = {username,ga.getAuthority()};
		int[] argTypes = {Types.VARCHAR, Types.VARCHAR};
		jdbc.update(SQL_INSERT_AUTHORITY,args,argTypes);
	}
	
}
