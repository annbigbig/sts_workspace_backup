package com.kashu.demo.utility.property.editors;

import java.beans.PropertyEditorSupport;

import com.kashu.demo.entity.Ingredient;

public class IngredientPropertyEditor extends PropertyEditorSupport {
	
	public String getAsText() {
		Ingredient ingredient = (Ingredient) getValue();
        return ingredient == null ? "" : ingredient.getId();
	}
	
	public void setAsText(String id) {
		setValue(new Ingredient(id, "", null));
	}

}
