package com.kashu.demo.entity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import lombok.Data;

@Data
public class User implements UserDetails {
	private static final long serialVersionUID = 1L;
	
	private String username;
	private String password;
	private Boolean enabled;
	private String fullname;
	private String street;
	private String city;
	private String state;
	private String zip;
	private String phoneNumber;
	private List<GrantedAuthority> authorities;
	
	public User() {
		
	}
	
	public User(String username, String password, Boolean enabled, String fullname
			, String street, String city, String state, String zip, String phoneNumber) {
		this.username = username;
		this.password = password;
		this.enabled = enabled;
		this.fullname = fullname;
		this.street = street;
		this.city = city;
		this.state = state;
		this.zip = zip;
		this.phoneNumber = phoneNumber;
		this.authorities = Arrays.asList(new SimpleGrantedAuthority("ROLE_USER"));
	}
	
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return authorities;
	}

	@Override
	public String getPassword() {
		return password;
	}

	@Override
	public String getUsername() {
		return username;
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return enabled;
	}

}
