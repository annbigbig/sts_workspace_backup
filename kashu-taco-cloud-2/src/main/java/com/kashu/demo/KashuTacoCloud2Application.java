package com.kashu.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KashuTacoCloud2Application {

	public static void main(String[] args) {
		SpringApplication.run(KashuTacoCloud2Application.class, args);
	}

}

